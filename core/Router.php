<?php

require_once 'Response.php';

class Router extends Response
{
    private $method;
    private $routes = [
        'GET' => [],
        'POST' => [],
        'PUT' => [],
        'DELETE' => []
    ];
    private $params = [
        'query' => [],
        'body' => [],
        'file' => []
    ];

    private $requestRoute = [];

    private $controller;

    private $action;

    public function __construct()
    {
        $this->initRoutes();
        $this->init();
        $this->start();
    }

    public function init()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->params['query'] = $_GET;
        if (!isset($this->params['query']['filter'])) $this->params['query']['filter'] = [];
        $this->params['file'] = $_FILES;
        $this->params['body'] = json_decode(file_get_contents('php://input'), true);
        if (empty($this->params['body'])) {
            $this->params['body'] = $this->parse_multipart();
        }
        $this->requestRoute = $_SERVER['REDIRECT_URL'];
        if (!isset($this->routes[$this->method][$this->requestRoute])) $this->JsonBadResponse(404, "Not found!");
        $this->controller = ucfirst($this->routes[$this->method][$this->requestRoute]['controller']) . 'Controller';
        $this->action = $this->routes[$this->method][$this->requestRoute]['action'];

        foreach ($this->routes[$this->method][$this->requestRoute]['RequiredBodyParams'] as $param) {
            if (empty($this->params['body'][$param])) $this->JsonBadResponse(400, "Need {$param}!");
        }
        if (isset($this->routes[$this->method][$this->requestRoute]['RequiredBodyFileParams'])) {
            foreach ($this->routes[$this->method][$this->requestRoute]['RequiredFile'] as $param) {
                if (empty($this->params['file'][$param])) $this->JsonBadResponse(400, "Need file {$param}!");
                $uploadDir = __DIR__ . '/../uploads/';
                $name = time();
                $uploadFile = $uploadDir . $name . basename($_FILES[$param]['name']);
                move_uploaded_file($_FILES[$param]['tmp_name'], $uploadFile);
            }
            $this->params['file'][$param] = $name . basename($_FILES[$param]['name']);
        }
    }

    public function initRoutes()
    {

        require_once __DIR__ . '/RouteList.php';
    }

    public function start()
    {

        if (!file_exists(__DIR__ . '/../controllers/' . $this->controller . '.php')) return null;

        include_once __DIR__ . '/../controllers/' . $this->controller . '.php';

        $controller = new $this->controller;

        $action = $this->action;

        $controller->$action($this->params);
    }

    function parse_multipart()
    {
        $data = [];

        $input = file_get_contents('php://input');

        preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
        $boundary = $matches[1];

        $blocks = preg_split("/-+$boundary/", $input);
        array_pop($blocks);

        foreach ($blocks as $id => $block) {
            if (empty($block))
                continue;

            if (strpos($block, 'application/octet-stream') !== FALSE) {
                preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
            } else {
                preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
            }
            $data[$matches[1]] = $matches[2];
        }
        return $data;
    }
}