<?php

$this->routes['GET']['/artist'] = [
    'controller' => 'Artist',
    'action' => 'index',
    'RequiredBodyParams' => []
];

$this->routes['POST']['/artist'] = [
    'controller' => 'Artist',
    'action' =>   'create',
    'RequiredBodyParams' => ['name']
];

$this->routes['PUT']['/artist'] = [
    'controller' => 'Artist',
    'action' =>   'update',
    'RequiredBodyParams' => ['id']
];

$this->routes['DELETE']['/artist'] = [
    'controller' => 'Artist',
    'action' =>   'delete',
    'RequiredBodyParams' => ['id']
];

$this->routes['PUT']['/album'] = [
    'controller' => 'Album',
    'action' =>   'update',
    'RequiredBodyParams' => ['id']
];

$this->routes['DELETE']['/album'] = [
    'controller' => 'Album',
    'action' =>   'delete',
    'RequiredBodyParams' => ['id']
];

$this->routes['GET']['/album'] = [
    'controller' => 'Album',
    'action' => 'index',
    'RequiredBodyParams' => []
];

$this->routes['POST']['/album'] = [
    'controller' => 'Album',
    'action' =>   'create',
    'RequiredBodyParams' => ['name','time','buyTime','price','artistId','key','year'],
    'RequiredFile' => ['thumb']
];