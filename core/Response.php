<?php

abstract class Response
{
    public function setHeaders($status = 200){
        header('Content-Type: application/json');
        http_response_code($status);
    }

    public function JsonBadResponse($status=400,$message=''){
        $this->setHeaders($status);

        $response = [
            'status' => $status,
            'message' => $message
        ];
        echo json_encode($response);
        exit();
    }

    public function JsonResponse($data=[]){
        $this->setHeaders();
        $response = [
            'status' => 200,
            'data' => $data,
        ];

        echo json_encode($response);

        exit();
    }

}