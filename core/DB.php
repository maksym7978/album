<?php

class DB
{
    private $port = 3306;

    private $dbName = 'tt';

    private $dbUser = 'root';

    private $dbPassword = '123123';

    private $dbHost = 'localhost';

    private $db;

    public function __construct()
    {
        $this->db = mysqli_connect($this->dbHost,$this->dbUser,$this->dbPassword,$this->dbName,$this->port);
        mysqli_set_charset($this->db,'utf8');

    }

    public function select($sql){
        $result = mysqli_query($this->db,$sql);
        $response = [];
        while( $row = mysqli_fetch_assoc($result) ){
            $response[] = $row;
        }

        mysqli_free_result($result);
        return $response;
    }

    public function insert($sql){
        $result = mysqli_query($this->db,$sql);
        return mysqli_insert_id($this->db);

    }

    public function execute($sql){
        $result = mysqli_query($this->db,$sql);

        return true;
    }


}