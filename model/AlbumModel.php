<?php
require_once 'BaseModel.php';
require_once 'ArtistModel.php';
class AlbumModel extends BaseModel
{
    static $table = 'albums';

    public $id;

    public $name;

    public $thumb;

    public $year;

    public $time;

    public $key;

    public $artistId;

    public $price;

    public $buyTime;

    public function __construct($id = false)
    {
        parent::__construct($id,self::$table);
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getThumb(){
        return $this->thumb;
    }

    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

    }
    public function getYear(){
        return $this->year;
    }

    public function setYear($year){
        $this->year = $year;
    }

    public function getTime(){
        return $this->time;
    }

    public function setTime($time){
        $this->time = $time;
    }

    public function getKey(){
        return $this->key;
    }

    public function setKey($key){
        $this->key = $key;
    }

    public function getArtistId(){
        return $this->artistId;
    }

    public function setArtistId($artistId){
        $this->artistId = $artistId;
    }

    public function getPrice(){
        return $this->price;
    }

    public function setPrice($price){
        $this->price = $price;
    }

    public function getBuyTime(){
        return $this->buyTime;
    }

    public function setBuyTime($buyTime){
        $this->buyTime = $buyTime;
    }


}