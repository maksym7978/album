<?php
include_once '../core/DB.php';
abstract class BaseModel
{
    private $create = false;

    private $db;

    private $table;

    public function __construct($id=false,$table)
    {
        $this->db = new DB();
        $this->table = $table;

        if(!$id){
            $this->create = true;
        }else{
            $sql = "SELECT * FROM {$this->table} WHERE id=$id LIMIT 1";
            $result = $this->db->select($sql);
            $vars = get_object_vars($this);
            if (count($vars) < 3) return;

            foreach ($vars as $field => $value) {
                if ($field == 'table' || $field == 'create' || $field == 'db'|| $field == 'typeFilter') continue;

                $this->$field = $result[0][$field];
            }
        }
    }

    public function save()
    {
        $vars = get_object_vars($this);
        if($this->create){


            $fieldSql = [];
            $valueSql = [];

            if (count($vars) < 3) return;

            foreach ($vars as $field => $value) {
                if ($field == 'table' || $field == 'id') continue;

                if (is_null($value) || empty($value)) return false;

                $fieldSql[] = $field;
                $valueSql[] = "'{$value}'";

            }
            $fieldSql = implode(',',$fieldSql);
            $valueSql = implode(',',$valueSql);
            $insertSql = "INSERT INTO `{$this->table}` ({$fieldSql}) SET ({$valueSql})";
            $this->id = $this->db->insert($insertSql);
        }else {
            $addSql = '1=1';
            if (count($vars) < 3) return;

            foreach ($vars as $field => $value) {
                if ($field == 'table' || $field == 'id' || $field == 'create' || $field == 'db' || $field == 'typeFilter') continue;

                if (is_null($value)) return false;

                    $addSql .= " AND {$field}='{$value}' ";

            }

            $updateSql = "UPDATE `{$this->table}` SET `{$addSql}` WHERE id={$this->id}";

            $this->db($updateSql);
        }
    }

    public function destroy()
    {
        $sql = "DELETE FROM {$this->table} WHERE id={$this->id}";
        $this->db->execute($sql);

    }

    public function export(){
        
        $vars = get_object_vars($this);
        
        $response = [];
        
        foreach ($vars as $field => $value) {
            if ($field == 'table' || $field == 'id' || $field == 'create' || $field == 'db'|| $field == 'typeFilter') continue;

            
            $response[$field] = $value;


        }
        return $response;
    }
    
    public function getTable(){
        return $this->table;
    }

    public function getAll($filters=[]){
        $filterSql = [];
        $table = $this->table;

        if(count($filters) < 1) {

            $sql = "SELECT * FROM {$table} ";

        }else{
            $vars = get_object_vars($this);

            foreach ($filters as $filter => $val){
                if(!array_key_exists($filter,$vars)) continue;

                $filterSql[] = " {$filter}='{$val}' ";

            }

            $filterSql = implode('AND',$filterSql);
            $sql = "SELECT * FROM {$table} WHERE {$filterSql}";
        }

        return $this->db->select($sql);
    }
}