<?php
require_once 'BaseModel.php';
class ArtistModel extends BaseModel
{
    static $table = 'artist';

    public $id;

    public $name;

    public function __construct($id = false)
    {
        parent::__construct($id,self::$table);
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }


}