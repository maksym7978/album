<?php

require_once __DIR__ . '/../model/AlbumModel.php';
require_once __DIR__ . '/../core/DB.php';

class AlbumController extends BaseController
{
    public function index($params)
    {
        $albumModel = new AlbumModel();
        $this->JsonResponse($albumModel->getAll($params['query']['filter']));

    }

    public function create($params)
    {
        $album = new AlbumModel();

        $album->setName($params['body']['name']);

        $album->setThumb($params['file']['thumb']);
        $album->setYear($params['body']['year']);
        $album->setTime($params['body']['time']);
        $album->setKey($params['body']['key']);
        $album->setArtistId($params['body']['artistId']);
        $album->setPrice($params['body']['price']);
        $album->setBuyTime($params['body']['buyTime']);
        $album->save();

        $this->JsonResponse($album->export());
    }

    public function update($params)
    {
        $album = new AlbumModel($params['body']['id']);

        (isset($params['body']['name'])) ? $album->setName($params['body']['name']) : false;
        (isset($params['file']['thumb'])) ? $album->setThumb($params['file']['thumb']) : false;
        (isset($params['body']['year'])) ? $album->setYear($params['body']['year']) : false;
        (isset($params['body']['time'])) ? $album->setTime($params['body']['time']) : false;
        (isset($params['body']['key'])) ? $album->setKey($params['body']['key']) : false;
        (isset($params['body']['artistId'])) ? $album->setArtistId($params['body']['artistId']) : false;
        (isset($params['body']['price'])) ? $album->setPrice($params['body']['price']) : false;
        (isset($params['body']['buyTime'])) ? $album->setBuyTime($params['body']['buyTime']) : false;
        $album->save();

        $this->JsonResponse($album->export());
    }

    public function delete($params)
    {
        $album = new AlbumModel($params['body']['id']);

        $album->destroy();

        $this->JsonResponse(['status' => 'success']);
    }

    public function getAll($filters = [])
    {
        $filterSql = [];
        $table = $this->table;

        if (count($filters) < 1) {

            $sql = "SELECT * FROM {$table} ";

        } else {
            $vars = get_object_vars($this);

            foreach ($filters as $filter => $val) {
                if (!array_key_exists($filter, $vars)) continue;

                $filterSql[] = " {$filter}='{$val}' ";

            }

            $filterSql = implode('AND', $filterSql);
            $sql = "SELECT * FROM {$table} WHERE {$filterSql}";
        }

        return $this->db->select($sql);
    }

}