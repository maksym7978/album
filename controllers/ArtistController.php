<?php
require_once 'BaseController.php';
require_once __DIR__.'/../model/ArtistModel.php';
require_once  __DIR__.'/../core/DB.php';

class ArtistController extends BaseController
{
    public function index($params){
        $artistModel = new ArtistModel();
        $this->JsonResponse($artistModel->getAll($params['query']['filter']));

    }

    public function create($params){
        $artist = new ArtistModel();

        $artist->setName($params['body']['name']);

        $artist->save();

        $this->JsonResponse($artist->export());
    }

    public function update($params){
        $artist = new ArtistModel($params['body']['id']);

        (isset($params['body']['name'])) ? $artist->setName($params['body']['name']) : false;

        $artist->save();

        $this->JsonResponse($artist->export());
    }

    public function delete($params){
        $artist = new ArtistModel($params['body']['id']);

        $artist->destroy();

        $this->JsonResponse(['status' => 'success']);
    }
}